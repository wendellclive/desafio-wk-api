package br.com.desafiowk.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.desafiowk.model.Doador;
import br.com.desafiowk.model.dto.CandidatoEstado;
import br.com.desafiowk.model.dto.ImcFaixaEtaria;
import br.com.desafiowk.model.dto.ObesosHomemMulher;
import br.com.desafiowk.model.dto.TipoSanguineo;
import br.com.desafiowk.repositories.DoadorRepository;

@Service
public class DoadorService {

	@Autowired
	private DoadorRepository repository;

	public void importarDados(String jsonUrl) throws IOException {

		if (!verificarSeJaImportados()) {

			// Realiza solicitação HTTP para obter o conteúdo JSON
			String jsonString = getJsonString(jsonUrl);

			// Analisa o JSON em objetos Java
			Gson gson = new Gson();
			Doador[] dadosArray = gson.fromJson(jsonString, Doador[].class);

			// Insere os dados no banco de dados
			repository.saveAll(Arrays.asList(dadosArray));

		}

	}

	private String getJsonString(String jsonUrl) throws IOException {

		URL url = new URL(jsonUrl);
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		StringBuilder stringBuilder = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
		}
		reader.close();
		return stringBuilder.toString();

	}

	public boolean verificarSeJaImportados() throws IOException {

		List<Doador> doador = new ArrayList<>();
		doador = repository.findAll();
		return doador.isEmpty() ? false : true;

	}

	public List<CandidatoEstado> candidatosPorEstado() {

		List<Object[]> obj = repository.buscarCandidatosPorEstado();
		List<CandidatoEstado> candidatos = new ArrayList<>();
		for (Object[] item : obj) {
			candidatos.add(new CandidatoEstado(item[0].toString(), Integer.parseInt(item[1].toString())));
		}
		return candidatos;

	}

	public List<ImcFaixaEtaria> imcPorFaixaEtaria() {

		List<Object[]> obj = repository.imcPorFaixaIdade();
		List<ImcFaixaEtaria> imcsFaixaEtaria = new ArrayList<>();
		for (Object[] item : obj) {
			imcsFaixaEtaria.add(new ImcFaixaEtaria(item[0].toString(), item[1].toString()));
		}
		return imcsFaixaEtaria;

	}

	public List<ObesosHomemMulher> obesoHomemMulher() {

		List<Object[]> obj = repository.obesosHomenMulher();
		List<ObesosHomemMulher> obesosHomemMulher = new ArrayList<>();
		for (Object[] item : obj) {
			obesosHomemMulher.add(new ObesosHomemMulher(item[0].toString(), item[1].toString(), item[2].toString(), item[3].toString()+"%"));
		}
		return obesosHomemMulher;
		
	}

	public List<TipoSanguineo> tipoSanguineo(String chamada) {
		
		List<Object[]> obj = chamada.equals("tipoSanguineoMedia") ? repository.mediaIdadeTipoSanguineo() : repository.possiveisDoadoresReceptor();
		List<TipoSanguineo> tiposSanguineos = new ArrayList<>();
		for (Object[] item : obj) {
			tiposSanguineos.add(new TipoSanguineo(item[0].toString(), item[1].toString()));
		}
		return tiposSanguineos;
	}


}
