package br.com.desafiowk.controller;

import static br.com.desafiowk.config.Constantes.DOADORES;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafiowk.model.dto.CandidatoEstado;
import br.com.desafiowk.model.dto.ImcFaixaEtaria;
import br.com.desafiowk.model.dto.ObesosHomemMulher;
import br.com.desafiowk.model.dto.TipoSanguineo;
import br.com.desafiowk.service.DoadorService;

@RestController
@RequestMapping(value = DOADORES, produces = MediaType.APPLICATION_JSON_VALUE)
public class DoadorController {

	@Autowired
	private DoadorService service;

	@PostMapping("/importar-dados")
	public ResponseEntity<String> importarDados() throws IOException {

		String jsonUrl = "https://communication-assets.gupy.io/production/companies/52441/emails/1686945524213/e8330670-6f23-11ed-91a8-05f5cf6759fb/data_1.json";
		service.importarDados(jsonUrl);
		return ResponseEntity.ok("Dados importados com sucesso.");
	}

	@GetMapping("/candidatos")
	public ResponseEntity<List<CandidatoEstado>> candidatosPorEstado() {
		return ResponseEntity.ok().body(service.candidatosPorEstado());
	}

	@GetMapping("/imcPorFaixaEtaria")
	public ResponseEntity<List<ImcFaixaEtaria>> imcPorFaixaEtaria() {
		return ResponseEntity.ok().body(service.imcPorFaixaEtaria());
	}

	@GetMapping("/obesoHomemMulher")
	public ResponseEntity<List<ObesosHomemMulher>> obesoHomemMulher() {
		return ResponseEntity.ok().body(service.obesoHomemMulher());
	}

	@GetMapping("/tipoSanguineoMedia")
	public ResponseEntity<List<TipoSanguineo>> tipoSanguineoMedia() {
		return ResponseEntity.ok().body(service.tipoSanguineo("tipoSanguineoMedia"));
	}

	@GetMapping("/possiveisDoadoresReceptor")
	public ResponseEntity<List<TipoSanguineo>> possiveisDoadoresReceptor() {
		return ResponseEntity.ok().body(service.tipoSanguineo(""));
	}

}
