package br.com.desafiowk.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CandidatoEstado {

	private String estado;
	private Integer totalUsuarios;
	
}
