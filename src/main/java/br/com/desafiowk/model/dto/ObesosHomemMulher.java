package br.com.desafiowk.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ObesosHomemMulher {

	private String sexo;
	private String total;
	private String obesos;
	private String percentual;
	
}
