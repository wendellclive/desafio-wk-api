package br.com.desafiowk.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TipoSanguineo {

	private String tipoSanguineo;
	private String valor;
	
}
