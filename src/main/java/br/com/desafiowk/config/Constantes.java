package br.com.desafiowk.config;

/***
 * Created by Wendell Clive - email: wendell.clive@gmail.com
 * 19/06/2023
 * */

public class Constantes {

	public static final String PATH_API = "/api";
	public static final String DOADORES = PATH_API + "/doador";
	
}