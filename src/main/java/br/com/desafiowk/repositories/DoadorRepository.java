package br.com.desafiowk.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.desafiowk.model.Doador;

@Repository
public interface DoadorRepository extends JpaRepository<Doador, Long> {

	@Query("SELECT estado, COUNT(*) AS totalUsuarios FROM Doador GROUP BY estado ORDER BY estado")
	List<Object[]> buscarCandidatosPorEstado();
	
	@Query(value = "SELECT FLOOR(DATEDIFF(NOW(), STR_TO_DATE(data_nasc, '%d/%m/%Y')) / 365 / 10) * 10 AS faixa_idade,  " + 
			       "ROUND(AVG(peso / (altura * altura)),2) AS imc_medio   FROM doador GROUP BY faixa_idade ORDER BY faixa_idade", nativeQuery = true) 
	List<Object[]> imcPorFaixaIdade();

	@Query(value = "SELECT sexo, COUNT(*) AS total, SUM(CASE WHEN (peso / (altura * altura)) > 30 THEN 1 ELSE 0 END) AS obesos, " +
		    	   "ROUND((SUM(CASE WHEN (peso / (altura * altura)) > 30 THEN 1 ELSE 0 END) / COUNT(*)) * 100, 2) AS percentual_obesos " +
		    	   "FROM doador GROUP BY sexo", nativeQuery = true)
	List<Object[]> obesosHomenMulher();

	@Query(value = "SELECT tipo_sanguineo, ROUND(AVG(FLOOR(DATEDIFF(NOW(), STR_TO_DATE(data_nasc, '%d/%m/%Y')) / 365)), 0) AS media_idade  " +
					"FROM doador GROUP BY tipo_sanguineo;", nativeQuery = true) 
	List<Object[]> mediaIdadeTipoSanguineo();
	
	@Query(value = "SELECT tipo_sanguineo,  " + 
			"       SUM(CASE   " + 
			"           WHEN tipo_sanguineo = 'A+' THEN  " + 
			"               CASE  " + 
			"                   WHEN tipo_sanguineo IN ('A+', 'A-', 'O+', 'O-') THEN 1  " + 
			"                   ELSE 0  " + 
			"               END  " + 
			"           WHEN tipo_sanguineo = 'A-' THEN  " + 
			"               CASE  " + 
			"                   WHEN tipo_sanguineo IN ('A-', 'O-') THEN 1  " + 
			"                   ELSE 0  " + 
			"               END  " + 
			"           WHEN tipo_sanguineo = 'B+' THEN  " + 
			"               CASE  " + 
			"                   WHEN tipo_sanguineo IN ('B+', 'B-', 'O+', 'O-') THEN 1  " + 
			"                   ELSE 0  " + 
			"               END  " + 
			"           WHEN tipo_sanguineo = 'B-' THEN  " + 
			"               CASE  " + 
			"                   WHEN tipo_sanguineo IN ('B-', 'O-') THEN 1  " + 
			"                   ELSE 0  " + 
			"               END  " + 
			"           WHEN tipo_sanguineo = 'AB+' THEN  " + 
			"               CASE  " + 
			"                   WHEN tipo_sanguineo IN ('A+', 'B+', 'O+', 'AB+', 'A-', 'B-', 'O-', 'AB-') THEN 1  " + 
			"                   ELSE 0  " + 
			"               END  " + 
			"           WHEN tipo_sanguineo = 'AB-' THEN  " + 
			"               CASE  " + 
			"                   WHEN tipo_sanguineo IN ('A-', 'B-', 'O-', 'AB-') THEN 1  " + 
			"                   ELSE 0  " + 
			"               END  " + 
			"           WHEN tipo_sanguineo = 'O+' THEN  " + 
			"               CASE  " + 
			"                   WHEN tipo_sanguineo IN ('O+', 'O-') THEN 1  " + 
			"                   ELSE 0  " + 
			"               END  " + 
			"           WHEN tipo_sanguineo = 'O-' THEN  " + 
			"               CASE  " + 
			"                   WHEN tipo_sanguineo = 'O-' THEN 1  " + 
			"                   ELSE 0  " + 
			"               END  " + 
			"           ELSE 0  " + 
			"       END) AS quantidade_doadores  " + 
			"FROM doador  " + 
			"WHERE FLOOR(DATEDIFF(NOW(), STR_TO_DATE(data_nasc, '%d/%m/%Y')) / 365) >= 16  " + 
			"  AND FLOOR(DATEDIFF(NOW(), STR_TO_DATE(data_nasc, '%d/%m/%Y')) / 365) <= 69  " + 
			"  AND peso > 50  " + 
			"GROUP BY tipo_sanguineo;", nativeQuery = true) 
	List<Object[]> possiveisDoadoresReceptor();
	
}
