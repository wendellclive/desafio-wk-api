package br.com.desafiowk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioWkApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioWkApplication.class, args);
	}

}
